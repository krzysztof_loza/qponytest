//
//  CurrencyExTests.swift
//  CurrencyExTests
//
//  Created by Krzysztof Loza on 06/04/2020.
//  Copyright © 2020 Syndicate Technologies. All rights reserved.
//

import XCTest
@testable import CurrencyEx

class CurrencyExTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testCurrencyTableViewModel() throws {
        let provider = NetworkProvider(stub: true)
        let model = CurrencyTablesViewModel(provider: provider)
        model.getTables { (success, error) in
            XCTAssert(success, "Request failed")
            XCTAssertNil(error, "Error not nil \(error?.localizedDescription ?? "")")
        }
    }

    func testHistoryTableViewModel() throws {
        let provider = NetworkProvider(stub: true)
        let model = CurrencyTablesViewModel(provider: provider)
        model.getTables { (success, error) in
            XCTAssert(success, "Request failed")
            XCTAssertNil(error, "Error not nil \(error?.localizedDescription ?? "")")
            let rate = model.rate(for: 0)
            XCTAssertNotNil(rate, "No rates")
            if let rate = rate {
                let historyModel = RatesHistoryViewModel(provider: provider, type: .A, currency: rate)
                historyModel.fromDate = Date()
                historyModel.toDate = Date()
                historyModel.getHistory { (success, error) in
                    XCTAssert(success, "Request failed")
                    XCTAssertNil(error, "Error not nil \(error?.localizedDescription ?? "")")
                }
            }
        }
    }

}
