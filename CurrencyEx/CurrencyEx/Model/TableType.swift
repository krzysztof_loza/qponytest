//
//  TableType.swift
//  CurrencyEx
//
//  Created by Krzysztof Loza on 06/04/2020.
//  Copyright © 2020 Syndicate Technologies. All rights reserved.
//

import Foundation

enum TableType: String, CaseIterable {
    case A = "A"
    case B = "B"
    case C = "C"
}
