//
//  Helpers.swift
//  CurrencyEx
//
//  Created by Krzysztof Loza on 06/04/2020.
//  Copyright © 2020 Syndicate Technologies. All rights reserved.
//

import Foundation

enum ViewControllerIDs {
    static let currencyTablesViewController = "CurrencyTablesViewController"
    static let ratesHistoryViewController = "RatesHistoryViewController"
    static let datePickerViewController = "DatePickerViewController"
}

enum CellIDs {
    static let currencyCellId = "CurrencyCellId"
    static let historyCellId = "HistoryCellId"
}
