//
//  ViewController.swift
//  CurrencyEx
//
//  Created by Krzysztof Loza on 06/04/2020.
//  Copyright © 2020 Syndicate Technologies. All rights reserved.
//

import UIKit
import MBProgressHUD

class CurrencyTablesViewController: BaseViewController {

    var viewModel: CurrencyTablesViewModel?
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmentedControl: SegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = NSLocalizedString("CurrencyTablesTitle", comment: "")
        let segmentA = SegmentDescription(title: "A", selectedColor: .blue)
        let segmentB = SegmentDescription(title: "B", selectedColor: .green)
        let segmentC = SegmentDescription(title: "C", selectedColor: .blue)
        segmentedControl.setSegments([segmentA, segmentB, segmentC])
        segmentedControl.delegate = self

        loadData()
    }

//MARK: - Buttons actions
    @IBAction func reload(_ sender: Any) {
        loadData()
    }

//MARK: Internal
    private func loadData() {
        MBProgressHUD.showAdded(to: view, animated: true)
        viewModel?.getTables { [unowned self] (success, error) in
            if success == true {
                self.tableView.reloadData()
            } else {
                self.handleError(error)
            }
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
}

//MARK: - UITableViewDataSource
extension CurrencyTablesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.numberOfRows() ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: CellIDs.currencyCellId, for: indexPath) as? CurrencyTableViewCell {
            if let rateModel = viewModel?.rateModel(for: indexPath.row) {
                cell.setup(model: rateModel)
            }
            return cell
        } else {
            fatalError()
        }
    }
}

//MARK: - UITableViewDelegate
extension CurrencyTablesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let rate = viewModel?.rate(for: indexPath.row), let section = viewModel?.selectedSection else {
            return
        }
        coordinator?.showRatesHistory(tableType: section, rate: rate)
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: CGRect.zero)
    }
}

//MARK: - SegmentDelegate
extension CurrencyTablesViewController: SegmentDelegate {
    func didSelect(index: Int) {
        viewModel?.setSelectedTable(index)
        tableView.reloadData()
    }
}

