//
//  RateHistoryTableViewCell.swift
//  CurrencyEx
//
//  Created by Krzysztof Loza on 06/04/2020.
//  Copyright © 2020 Syndicate Technologies. All rights reserved.
//

import UIKit

class RateHistoryTableViewCell: UITableViewCell {
    @IBOutlet weak var effectiveDateLabel: UILabel!
    @IBOutlet weak var rateLabel: UILabel!

    func setup(model: RateViewModel) {
        effectiveDateLabel.text = model.effectiveDate
        rateLabel.text = model.rateValue
    }

}
