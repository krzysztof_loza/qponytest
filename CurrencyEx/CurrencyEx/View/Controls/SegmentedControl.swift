//
//  SegmentedControl.swift
//  CurrencyEx
//
//  Created by Krzysztof Loza on 06/04/2020.
//  Copyright © 2020 Syndicate Technologies. All rights reserved.
//

import Foundation
import UIKit

protocol SegmentDelegate: AnyObject {
    func didSelect(index: Int)
}

class SegmentDescription {
    var backgroundColor: UIColor = .white
    var selectedColor: UIColor = .lightGray
    var title: String?
    var button: UIButton?

    init(title: String?, color: UIColor = .lightGray, selectedColor: UIColor = .cyan) {
        self.title = title
        self.backgroundColor = color
        self.selectedColor = selectedColor
    }
}

class SegmentedControl: UIView {

    private var segmentDescriptions = [SegmentDescription]()
    private(set) var selectedSegment: Int = 0
    private var stackView: UIStackView?

    weak var delegate: SegmentDelegate?

    var numberOfSegments: Int {
        get {
            return segmentDescriptions.count
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        initView()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initView()
    }

    private func initView() {
        stackView = UIStackView(frame: frame)
        stackView?.alignment = .fill
        stackView?.axis = .horizontal
        stackView?.distribution = .fillEqually
        if let stack = stackView {
            addSubview(stack)
        }
        stackView?.translatesAutoresizingMaskIntoConstraints = false
        stackView?.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        stackView?.topAnchor.constraint(equalTo: topAnchor).isActive = true
        stackView?.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        stackView?.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
    }

//MARK: - Public interface
    func setSegments(_ segments: [SegmentDescription]) {
        segmentDescriptions.removeAll()
        segmentDescriptions.append(contentsOf: segments)
        if selectedSegment >= numberOfSegments {
            selectedSegment = 0
        }
        for i in 0..<numberOfSegments {
            let description = segmentDescriptions[i]
            let segment = UIButton(type: .custom)
            segment.addTarget(self, action: #selector(didSelect(sender:)), for: .touchUpInside)
            description.button = segment
            stackView?.addArrangedSubview(segment)
        }
        updateSegments()
    }

    func addSegment(_ segment: SegmentDescription) {
        let button = UIButton(type: .custom)
        button.addTarget(self, action: #selector(didSelect(sender:)), for: .touchUpInside)
        segment.button = button
        segmentDescriptions.append(segment)
        stackView?.addArrangedSubview(button)
        updateSegments()
    }

    func setTitle(_ title: String?, for index: Int) {
        if segmentDescriptions.indices.contains(index) {
            segmentDescriptions[index].title = title
        }
    }

//MARK: - Internal
    private func updateSegments() {
        for i in 0..<numberOfSegments {
            let description = segmentDescriptions[i]
            let segment = description.button
            segment?.setTitle(description.title, for: .normal)
            if i == selectedSegment {
                segment?.backgroundColor = description.selectedColor
                segment?.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20.0)
            } else {
                segment?.backgroundColor = description.backgroundColor
                segment?.titleLabel?.font = UIFont.systemFont(ofSize: 16.0)
            }
        }
    }

    @objc private func didSelect(sender: UIButton) {
        if let toSelect = segmentDescriptions.firstIndex(where: {$0.button == sender}) {
            selectedSegment = toSelect
            updateSegments()
            delegate?.didSelect(index: selectedSegment)
        }
    }

}
