//
//  CurrencyTableViewCell.swift
//  CurrencyEx
//
//  Created by Krzysztof Loza on 06/04/2020.
//  Copyright © 2020 Syndicate Technologies. All rights reserved.
//

import UIKit

class CurrencyTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var effectiveDateLabel: UILabel!
    @IBOutlet weak var rateLabel: UILabel!

    func setup(model: RateViewModel) {
        nameLabel.text = model.name
        codeLabel.text = model.code
        effectiveDateLabel.text = model.effectiveDate
        rateLabel.text = model.rateValue
    }

}
