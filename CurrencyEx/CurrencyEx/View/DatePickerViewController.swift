//
//  DatePickerViewController.swift
//  CurrencyEx
//
//  Created by Krzysztof Loza on 07/04/2020.
//  Copyright © 2020 Syndicate Technologies. All rights reserved.
//

import UIKit

typealias DatePickerCompletion = (Date?)->Void

class DatePickerViewController: BaseViewController {
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var selectButton: UIBarButtonItem!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var completionBlock: DatePickerCompletion?
    var minDate: Date?
    var maxDate: Date?

    override func viewDidLoad() {
        super.viewDidLoad()
        selectButton.title = NSLocalizedString("SelectDate", comment: "")
        cancelButton.title = NSLocalizedString("Cancel", comment: "")
        datePicker.minimumDate = minDate
        datePicker.maximumDate = maxDate
    }

//MARK: - Buttons actions
    @IBAction func cancel(_ sender: Any) {
        if let completion = completionBlock {
            completion(nil)
        }
        coordinator?.dismissDatePicker(self)
    }

    @IBAction func selectDate(_ sender: Any) {
        if let completion = completionBlock {
            completion(datePicker.date)
        }
        coordinator?.dismissDatePicker(self)
    }

}
