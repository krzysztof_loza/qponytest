//
//  RateHistoryViewController.swift
//  CurrencyEx
//
//  Created by Krzysztof Loza on 06/04/2020.
//  Copyright © 2020 Syndicate Technologies. All rights reserved.
//

import UIKit
import MBProgressHUD

class RateHistoryViewController: BaseViewController {

    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var fromDateLabel: UILabel!
    @IBOutlet weak var toLabel: UILabel!
    @IBOutlet weak var toDateLabel: UILabel!
    @IBOutlet weak var showHistoryButton: UIButton!
    @IBOutlet weak var historyTableView: UITableView!

    var viewModel: RatesHistoryViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        fromLabel.text = NSLocalizedString("From", comment: "")
        toLabel.text = NSLocalizedString("To", comment: "")
        showHistoryButton.setTitle(NSLocalizedString("ShowHistory", comment: ""), for: .normal)

        if viewModel?.fromDateString == nil {
            fromDateLabel.text = NSLocalizedString("SelectDate", comment: "")
        }
        if viewModel?.toDateString == nil {
            toDateLabel.text = NSLocalizedString("SelectDate", comment: "")
        }
        title = viewModel?.currencyName
        enableShowButton()
    }

// MARK: - Internal
    fileprivate func fromChanged(newDate: Date) {
        viewModel?.fromDate = newDate
        fromDateLabel.text = viewModel?.fromDateString
        enableShowButton()
    }

    fileprivate func toChanged(newDate: Date) {
        viewModel?.toDate = newDate
        toDateLabel.text = viewModel?.toDateString
        enableShowButton()
    }

    fileprivate func enableShowButton() {
        if let _ = viewModel?.fromDate, let _ = viewModel?.toDate {
            showHistoryButton.isEnabled = true
        } else {
            showHistoryButton.isEnabled = false
        }
    }

//MARK: - Buttons actions
    @IBAction func didTapFromLabel(_ sender: Any) {
        coordinator?.showDatePicker(from: self, minDate: viewModel?.fromMinimumDate, maxDate: viewModel?.fromMaximumDate, completion: { [weak self] (date) in
            if let newDate = date {
                self?.fromChanged(newDate: newDate)
            }
        })
    }

    @IBAction func didTapToLabel(_ sender: Any) {
        coordinator?.showDatePicker(from: self, minDate: viewModel?.toMinimumDate, maxDate: viewModel?.toMaximumDate, completion: { [weak self] (date) in
            if let newDate = date {
                self?.toChanged(newDate: newDate)
            }
        })
    }

    @IBAction func showHistory(_ sender: Any) {
        MBProgressHUD.showAdded(to: view, animated: true)
        viewModel?.getHistory(completion: { [unowned self] (success, error) in
            if success == true {
                self.historyTableView.reloadData()
            } else {
                self.handleError(error)
            }
            MBProgressHUD.hide(for: self.view, animated: true)
        })
    }
}

//MARK: - UITableViewDataSource
extension RateHistoryViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.numberOfRows() ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: CellIDs.historyCellId, for: indexPath) as? RateHistoryTableViewCell {
            if let rateModel = viewModel?.rateModel(for: indexPath.row) {
                cell.setup(model: rateModel)
            }
            return cell
        } else {
            fatalError()
        }
    }
}

//MARK: - UITableViewDelegate
extension RateHistoryViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: CGRect.zero)
    }
}
