//
//  BaseViewController.swift
//  CurrencyEx
//
//  Created by Krzysztof Loza on 06/04/2020.
//  Copyright © 2020 Syndicate Technologies. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    weak var coordinator: Coordinator?

    func handleError(_ error: Error?) {
        var message = ""
        if let err = error as? NetworkError {
            switch err {
            case .noNetwork:
                message = NSLocalizedString("NoNetworkErrorMessage", comment: "")
            case .parse:
                message = NSLocalizedString("ParseErrorMessage", comment: "")
            case .request(let underlying):
                let format = NSLocalizedString("RequestErrorMessage", comment: "")
                message = String(format: format, underlying.localizedDescription)
            }
        } else if let err = error {
            message = err.localizedDescription
        } else {
            message = NSLocalizedString("GenericErrorMessage", comment: "")
        }
        let alert = UIAlertController(title: NSLocalizedString("ErrorTitle", comment: ""), message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
