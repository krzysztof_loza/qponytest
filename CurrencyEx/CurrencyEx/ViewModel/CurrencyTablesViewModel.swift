//
//  TablesViewModel.swift
//  CurrencyEx
//
//  Created by Krzysztof Loza on 06/04/2020.
//  Copyright © 2020 Syndicate Technologies. All rights reserved.
//

import Foundation

struct Tables {
    let type: TableType
    let table: CurrencyTable
}

class CurrencyTablesViewModel {
    private let provider: NetworkProvider
    private(set) var tables = [TableType: CurrencyTable]()
    private(set) var selectedSection: TableType = .A

    init(provider: NetworkProvider) {
        self.provider = provider
    }

    func getTables(completion: @escaping (Bool, Error?)->Void) {
        let group = DispatchGroup()
        var success = true
        var error: Error?

        TableType.allCases.forEach {[unowned self] tableType in
            group.enter()
            provider.getTable(type: tableType) { (result) in
                switch result {
                case .success(let tables):
                    if let table = tables.first {
                        self.tables[tableType] = table
                    }
                case .failure(let err):
                    error = err
                    success = false
                }
                group.leave()
            }
        }

        group.notify(queue: DispatchQueue.main) {
            completion(success, error)
        }
    }

    func setSelectedTable(_ index: Int) {
        if TableType.allCases.indices.contains(index) {
            selectedSection = TableType.allCases[index]
        }
    }

    func rate(for index: Int) -> Rate? {
        guard let rate = tables[selectedSection]?.rates?[index] else {
            return nil
        }
        return rate
    }
}

//MARK: - TableViewDataSource
extension CurrencyTablesViewModel: TableViewDataSource {

    func numberOfRows() -> Int {
        return tables[selectedSection]?.rates?.count ?? 0
    }

    func rateModel(for index: Int) -> RateViewModel? {
        guard let rate = tables[selectedSection]?.rates?[index] else {
            return nil
        }
        return RateViewModel(rate: rate, date: tables[selectedSection]?.effectiveDate)
    }

}
