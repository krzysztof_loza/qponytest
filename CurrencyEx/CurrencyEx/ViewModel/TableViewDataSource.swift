//
//  TableViewDataSource.swift
//  CurrencyEx
//
//  Created by Krzysztof Loza on 07/04/2020.
//  Copyright © 2020 Syndicate Technologies. All rights reserved.
//

import Foundation

protocol TableViewDataSource {
    func numberOfRows() -> Int
    func rateModel(for index: Int) -> RateViewModel?
}
