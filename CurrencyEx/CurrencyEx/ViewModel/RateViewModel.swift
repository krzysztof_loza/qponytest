//
//  RateViewModel.swift
//  CurrencyEx
//
//  Created by Krzysztof Loza on 06/04/2020.
//  Copyright © 2020 Syndicate Technologies. All rights reserved.
//

import Foundation

class RateViewModel {
    let rate: Rate
    let date: String?

    var name: String? {
        get {
            return rate.currency
        }
    }

    var code: String? {
        get {
            return rate.code
        }
    }

    var effectiveDate: String? {
        get {
            return date
        }
    }

    var rateValue: String {
        get {
            if let mid = rate.mid {
                return String(format: "%.4f", mid)
            }
            guard let bid = rate.bid, let ask = rate.ask else {
                return "0.0"
            }
            let value = (bid + ask) / 2.0
            return String(format: "%.4f", value)
        }
    }

    init(rate: Rate, date: String?) {
        self.rate = rate
        self.date = date
    }
}
