//
//  RatesHistoryViewModel.swift
//  CurrencyEx
//
//  Created by Krzysztof Loza on 06/04/2020.
//  Copyright © 2020 Syndicate Technologies. All rights reserved.
//

import Foundation

class RatesHistoryViewModel {
    private let tableType: TableType
    private let currency: Rate
    private let provider: NetworkProvider
    private lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "rrrr-MM-dd"
        return formatter
    }()

    private var history: CurrencyTable?
    var fromDate: Date?
    var toDate: Date?

    var fromMinimumDate: Date? {
        get {
            guard let to = toDate else {
                return nil
            }
            return to.addingTimeInterval(-yearTimeInterval())
        }
    }

    var fromMaximumDate: Date? {
        get {
            guard let to = toDate else {
                return Date()
            }
            return to
        }
    }

    var toMinimumDate: Date? {
        get {
            guard let from = fromDate else {
                return nil
            }
            return from
        }
    }

    var toMaximumDate: Date? {
        get {
            guard let from = fromDate else {
                return Date()
            }
            return min(Date(), from.addingTimeInterval(yearTimeInterval()))
        }
    }

    var fromDateString: String? {
        get {
            return dateString(date: fromDate)
        }
    }

    var toDateString: String? {
        get {
            return dateString(date: toDate)
        }
    }

    var currencyName: String? {
        return currency.currency
    }

    private func dateString(date: Date?) -> String? {
        guard let d = date else {
            return nil
        }
        return dateFormatter.string(from: d)
    }

    init(provider: NetworkProvider, type: TableType, currency: Rate) {
        self.provider = provider
        tableType = type
        self.currency = currency
    }

    func getHistory(completion: @escaping (Bool, Error?)->Void) {
        guard let fromDate = fromDate, let toDate = toDate, let code = currency.code else {
            completion(false, nil)
            return
        }
        let from = dateFormatter.string(from: fromDate)
        let to = dateFormatter.string(from: toDate)
        provider.getHistory(type: tableType, currency: code, from: from, to: to) { [unowned self] (result) in
            switch result {
            case .success(let table):
                self.history = table
                completion(true, nil)
            case .failure(let error):
                completion(false, error)
            }
        }
    }
}

//MARK: - TableViewDataSource
extension RatesHistoryViewModel: TableViewDataSource {
    func numberOfRows() -> Int {
        return history?.rates?.count ?? 0
    }

    func rateModel(for index: Int) -> RateViewModel? {
        guard let rate = history?.rates?[index] else {
            return nil
        }
        return RateViewModel(rate: rate, date: history?.rates?[index].effectiveDate)
    }
}
