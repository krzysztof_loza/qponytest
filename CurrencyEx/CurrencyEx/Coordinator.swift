//
//  Coordinator.swift
//  CurrencyEx
//
//  Created by Krzysztof Loza on 06/04/2020.
//  Copyright © 2020 Syndicate Technologies. All rights reserved.
//

import Foundation
import UIKit

class Coordinator {

    var navigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        guard let viewCtrl = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(identifier: ViewControllerIDs.currencyTablesViewController) as? CurrencyTablesViewController else {
            return
        }
        viewCtrl.coordinator = self
        let viewModel = CurrencyTablesViewModel(provider: NetworkProvider())
        viewCtrl.viewModel = viewModel
        navigationController.pushViewController(viewCtrl, animated: false)
    }

    func showRatesHistory(tableType: TableType, rate: Rate) {
        guard let viewCtrl = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(identifier: ViewControllerIDs.ratesHistoryViewController) as? RateHistoryViewController else {
            return
        }
        viewCtrl.coordinator = self
        viewCtrl.viewModel = RatesHistoryViewModel(provider: NetworkProvider(), type: tableType, currency: rate)
        navigationController.pushViewController(viewCtrl, animated: true)
    }

    func showDatePicker(from: UIViewController, minDate: Date?, maxDate: Date?, completion: DatePickerCompletion?) {
        guard let viewCtrl = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(identifier: ViewControllerIDs.datePickerViewController) as? DatePickerViewController else {
            return
        }
        viewCtrl.coordinator = self
        viewCtrl.minDate = minDate
        viewCtrl.maxDate = maxDate
        viewCtrl.completionBlock = completion
        from.present(viewCtrl, animated: true, completion: nil)
    }

    func dismissDatePicker(_ picker: DatePickerViewController) {
        picker.presentingViewController?.dismiss(animated: true, completion: nil)
    }
}
