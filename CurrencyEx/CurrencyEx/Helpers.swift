//
//  Helpers.swift
//  CurrencyEx
//
//  Created by Krzysztof Loza on 06/04/2020.
//  Copyright © 2020 Syndicate Technologies. All rights reserved.
//

import Foundation

func weekAgo() -> Date {
    let week: TimeInterval = 60.0*60.0*24.0*6.0
    return Date(timeIntervalSinceNow: -week)
}

func yearTimeInterval() -> TimeInterval {
    let year: TimeInterval = 60.0*60.0*24.0*366
    return year
}
