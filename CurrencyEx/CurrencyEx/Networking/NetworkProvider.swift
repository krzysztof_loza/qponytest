//
//  NetworkProvider.swift
//  CurrencyEx
//
//  Created by Krzysztof Loza on 06/04/2020.
//  Copyright © 2020 Syndicate Technologies. All rights reserved.
//

import Foundation
import Moya
import Reachability

enum NetworkError: Error {
    case noNetwork
    case request(underlying: Error)
    case parse
}

enum RequestResult<T> {
    case success(T)
    case failure(NetworkError)
}

class NetworkProvider {
    func networkConnected() -> Bool {
        return (try? Reachability().connection != .unavailable) ?? false
    }

    private let provider: MoyaProvider<NBPApi>

    init(stub: Bool = false) {
        provider = MoyaProvider<NBPApi>(stubClosure: stub ? MoyaProvider.immediatelyStub : MoyaProvider.neverStub)
    }

    fileprivate func performRequest<T:Codable>(_ request: NBPApi, resultType: T.Type, completion: @escaping (RequestResult<T>) -> Void) {
        if networkConnected() != true {
            completion(.failure(.noNetwork))
            return
        }
        provider.request(request) { (result) in
            switch result {
            case .success(let response):
                guard let decodedResponse = try? response.map(resultType.self) else {
                    print("Failed to parse response:\n\(String(data: response.data, encoding: .utf8) ?? "")")
                    completion(.failure(.parse))
                    return
                }
                print("Response:\n\(String(data: response.data, encoding: .utf8) ?? "")")
                completion(.success(decodedResponse))
            case .failure(let error):
                print(result)
                completion(.failure(.request(underlying: error)))
                return
            }

        }
    }

//MARK: - Public interface
    func getTable(type: TableType, completion: @escaping (RequestResult<[CurrencyTable]>) -> Void) {
        performRequest(.tables(table: type), resultType: [CurrencyTable].self, completion: completion)
    }

    func getHistory(type: TableType, currency: String, from: String, to: String, completion: @escaping (RequestResult<CurrencyTable>) -> Void) {
        performRequest(.currency(table: type, code: currency, from: from, to: to), resultType: CurrencyTable.self, completion: completion)
    }
}
