//
//  NBPApi.swift
//  CurrencyEx
//
//  Created by Krzysztof Loza on 06/04/2020.
//  Copyright © 2020 Syndicate Technologies. All rights reserved.
//

import Foundation
import Moya

fileprivate let apiURL = "https://api.nbp.pl/api"

enum NBPApi {
    case tables(table: TableType)
    case currency(table: TableType, code: String, from: String, to: String)
}

extension NBPApi: TargetType {
    var baseURL: URL {
        guard let url = URL(string: apiURL) else {
            fatalError()
        }
        return url
    }

    var path: String {
        switch self {
        case .tables(let table):
            return "/exchangerates/tables/\(table.rawValue)"
        case .currency(let table, let code, let from, let to):
            return "/exchangerates/rates/\(table.rawValue)/\(code)/\(from)/\(to)"
        }
    }

    var method: Moya.Method {
        switch self {
        case .tables, .currency:
            return .get
        }
    }

    var task: Task {
        switch self {
        case .tables, .currency:
            return .requestPlain
        }
    }

    var headers: [String : String]? {
        switch self {
        case .tables, .currency:
            return ["Accept":"application/json"]
        }
    }

    var sampleData: Data {
        var jsonString = ""
        switch self {
        case .tables:
            jsonString = """
            [{"table":"A","no":"067/A/NBP/2020","effectiveDate":"2020-04-06","rates":[{"currency":"bat (Tajlandia)","code":"THB","mid":0.1285},{"currency":"dolar amerykański","code":"USD","mid":4.2257},{"currency":"dolar australijski","code":"AUD","mid":2.5614},{"currency":"dolar Hongkongu","code":"HKD","mid":0.5451},{"currency":"dolar kanadyjski","code":"CAD","mid":2.9916},{"currency":"dolar nowozelandzki","code":"NZD","mid":2.5005},{"currency":"dolar singapurski","code":"SGD","mid":2.9451},{"currency":"euro","code":"EUR","mid":4.5612},{"currency":"forint (Węgry)","code":"HUF","mid":0.012554},{"currency":"frank szwajcarski","code":"CHF","mid":4.3199},{"currency":"funt szterling","code":"GBP","mid":5.2030},{"currency":"hrywna (Ukraina)","code":"UAH","mid":0.1551},{"currency":"jen (Japonia)","code":"JPY","mid":0.038703},{"currency":"korona czeska","code":"CZK","mid":0.1648},{"currency":"korona duńska","code":"DKK","mid":0.6107},{"currency":"korona islandzka","code":"ISK","mid":0.029332},{"currency":"korona norweska","code":"NOK","mid":0.4026},{"currency":"korona szwedzka","code":"SEK","mid":0.4153},{"currency":"kuna (Chorwacja)","code":"HRK","mid":0.5982},{"currency":"lej rumuński","code":"RON","mid":0.9443},{"currency":"lew (Bułgaria)","code":"BGN","mid":2.3321},{"currency":"lira turecka","code":"TRY","mid":0.6232},{"currency":"nowy izraelski szekel","code":"ILS","mid":1.1652},{"currency":"peso chilijskie","code":"CLP","mid":0.004882},{"currency":"peso filipińskie","code":"PHP","mid":0.0833},{"currency":"peso meksykańskie","code":"MXN","mid":0.1673},{"currency":"rand (Republika Południowej Afryki)","code":"ZAR","mid":0.2223},{"currency":"real (Brazylia)","code":"BRL","mid":0.7898},{"currency":"ringgit (Malezja)","code":"MYR","mid":0.9673},{"currency":"rubel rosyjski","code":"RUB","mid":0.0553},{"currency":"rupia indonezyjska","code":"IDR","mid":0.00025747},{"currency":"rupia indyjska","code":"INR","mid":0.05555},{"currency":"won południowokoreański","code":"KRW","mid":0.003437},{"currency":"yuan renminbi (Chiny)","code":"CNY","mid":0.5956},{"currency":"SDR (MFW)","code":"XDR","mid":5.7448}]}]
            """
        case .currency:
            jsonString = """
            {"table":"A","currency":"bat (Tajlandia)","code":"THB","rates":[{"no":"064/A/NBP/2020","effectiveDate":"2020-04-01","mid":0.1263},{"no":"065/A/NBP/2020","effectiveDate":"2020-04-02","mid":0.1271},{"no":"066/A/NBP/2020","effectiveDate":"2020-04-03","mid":0.1287},{"no":"067/A/NBP/2020","effectiveDate":"2020-04-06","mid":0.1285}]}
            """
        }
        return jsonString.data(using: .utf8) ?? Data()
    }

}
